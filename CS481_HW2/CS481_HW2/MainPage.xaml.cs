﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW2
{
    public partial class MainPage : ContentPage
    {

        long num1 = 0;
        long num2 = 0;
        string op = "-1";
        long ans = 0;

        public MainPage()
        {
            InitializeComponent();
        }

        private void Num7_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 7;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 7;
            }

        }

        private void Num8_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 8;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 8;
            }

        }

        private void Num9_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 9;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 9;
            }

        }

        private void Mult_Click(object sender, EventArgs e)
        {
            op = "*";
        }



        private void Num4_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 4;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 4;
            }

        }

        private void Num5_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 5;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 5;
            }

        }

        private void Num6_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 6;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 6;
            }

        }

        private void Sub_Click(object sender, EventArgs e)
        {
            op = "-";
        }



        private void Num1_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 1;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 1;
            }

        }

        private void Num2_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 2;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 2;
            }

        }

        private void Num3_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 3;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 3;
            }

        }

        private void Add_Click(object sender, EventArgs e)
        {
            op = "+";
        }

        private void Num0_Click(object sender, EventArgs e)
        {

            if (op == "-1")
            {
                num1 = 0;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = 0;
            }

        }


        private void Equal_Click(object sender, EventArgs e)
        {
            if (String.Equals(op, "*"))
            {
                ans = num1 * num2;
                display.Text = ans.ToString();
            }
            if (String.Equals(op, "-"))
            {
                ans = num1 - num2;
                display.Text = ans.ToString();
            }
            if (String.Equals(op, "+"))
            {
                ans = num1 + num2;
                display.Text = ans.ToString();
            }

            num1 = ans;
            op = "-1";
        }

        private void Clear_Click(object sender, EventArgs e)
        {
            num1 = 0;
            num2 = 0;
            op = "-1";
            ans = 0;
            display.Text = num1.ToString();
        }

        private void np_Click(object sender, EventArgs e)
        {
            if (op == "-1")
            {
                num1 = num1 * -1;
                display.Text = num1.ToString();
            }
            else
            {
                num2 = num2 * -1;
                display.Text = num2.ToString();
            }
        }
    }
}
